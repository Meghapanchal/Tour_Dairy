import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TourdiaryspecialPage } from './tourdiaryspecial';

@NgModule({
  declarations: [
    TourdiaryspecialPage,
  ],
  imports: [
    IonicPageModule.forChild(TourdiaryspecialPage),
  ],
})
export class TourdiaryspecialPageModule {}
