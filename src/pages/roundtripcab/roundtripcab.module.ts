import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoundtripcabPage } from './roundtripcab';

@NgModule({
  declarations: [
    RoundtripcabPage,
  ],
  imports: [
    IonicPageModule.forChild(RoundtripcabPage),
  ],
})
export class RoundtripcabPageModule {}
