import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { OnewaycabPage } from '../onewaycab/onewaycab';

@IonicPage()
@Component({
  selector: 'page-roundtripcab',
  templateUrl: 'roundtripcab.html',
})
export class RoundtripcabPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RoundtripcabPage');
  }
  oneway(){
    this.navCtrl.setRoot(OnewaycabPage);
  }

  roundtrip(){
    this.navCtrl.setRoot(RoundtripcabPage);
  }
}
