import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestApiProvider } from "../../providers/rest-api/rest-api";
import { OnewayPage } from '../oneway/oneway';
import { RoundtripPage } from '../roundtrip/roundtrip';
import { MulticityPage } from '../multicity/multicity';
import 'rxjs/Rx';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { FlightSearchResultPage } from '../flight-search-result/flight-search-result';

@IonicPage()
@Component({
  selector: 'page-flights',
  templateUrl: 'flights.html',
})
export class FlightsPage {

  flight: any;
  public searchFlight: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public api: RestApiProvider, public formBuilder: FormBuilder) {
    this.searchFlight = formBuilder.group({
      from: ['', Validators.compose([Validators.required])],
      to: ['', Validators.compose([Validators.required])],
      departure: ['', Validators.compose([Validators.required])],
      person: ['', Validators.compose([Validators.required])],
      class: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FlightsPage');
    this.api.getFlight().then(data =>{
      this.flight = data;
      console.log("data : ",this.flight.json().flight1)
    })
  }

    oneway(){
      this.navCtrl.setRoot(OnewayPage);
    }

    roundtrip(){
      this.navCtrl.setRoot(RoundtripPage);
    }

  multicity(){
    this.navCtrl.setRoot(MulticityPage);
  }

  search() {
    console.log("Search");
    this.api.getFlight().then(data =>{
      this.flight = data;
      if(this.searchFlight.value.from == this.flight.json().flight1.from && this.searchFlight.value.to == this.flight.json().flight1.to) {
        console.log("success !!!");
        this.navCtrl.push(FlightSearchResultPage);
      }
      else {
        console.log("no data found ... ")
      }
    });
  }
}
    
