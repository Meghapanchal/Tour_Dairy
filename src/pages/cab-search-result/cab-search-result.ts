import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, Platform } from 'ionic-angular';
import { AlertController, PopoverController, Popover , App, FabContainer, ItemSliding, List, ModalController, ToastController, LoadingController, Refresher } from 'ionic-angular';
import { DataBaseProvider } from "../../providers/data-base/data-base";
import { HomePage } from '../home/home';
@IonicPage()
@Component({
  selector: 'page-cab-search-result',
  templateUrl: 'cab-search-result.html',
})
export class CabSearchResultPage {
  userFeeds: any;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public app: App,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    // public confData: ConferenceData,
    public popoverCtrl: PopoverController,
    public platform: Platform,
    private DB: DataBaseProvider) {
  }

  ionViewDidEnter() {
    this.platform.ready()
   .then(()=>{
     this.loadFeeds();
   });
 }


  ionViewDidLoad() {
    console.log('ionViewDidLoad CabSearchResultPage');
  }
  loadFeeds() {
    this.userFeeds = this.DB.renderFeeds();
    console.log(this.DB.renderFeeds());
  }

  presentPopover(event: Event) {
    let popover = this.popoverCtrl.create(HomePage);
    popover.present({ ev: event });
  }

}
