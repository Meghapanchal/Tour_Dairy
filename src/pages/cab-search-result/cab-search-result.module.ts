import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CabSearchResultPage } from './cab-search-result';

@NgModule({
  declarations: [
    CabSearchResultPage,
  ],
  imports: [
    IonicPageModule.forChild(CabSearchResultPage),
  ],
})
export class CabSearchResultPageModule {}
