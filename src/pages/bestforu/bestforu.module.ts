import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BestforuPage } from './bestforu';

@NgModule({
  declarations: [
    BestforuPage,
  ],
  imports: [
    IonicPageModule.forChild(BestforuPage),
  ],
})
export class BestforuPageModule {}
