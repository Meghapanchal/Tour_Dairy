import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HoneymoonPage } from './honeymoon';

@NgModule({
  declarations: [
    HoneymoonPage,
  ],
  imports: [
    IonicPageModule.forChild(HoneymoonPage),
  ],
})
export class HoneymoonPageModule {}
