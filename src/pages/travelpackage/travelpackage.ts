import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

@IonicPage()
@Component({
  selector: 'page-travelpackage',
  templateUrl: 'travelpackage.html',
})
export class TravelpackagePage {

  // items = [
  //   'Ahmedabad',
  //   'Mehsana',
  //   'Rajkot',
  //   'Bhavanagr',
  //   'Surat',
  //   'Vadodara',
  //   'Kheda',
  //   'Palanpur',
  //   'Himmatnagar',
  //   'Jamnagar',
  //   'Junagadh',
  //   'Kutch',
  //   'Surendranagar',
  //   'Amreli',
  //   'Valsad',
  //   'Bharuch',
  //   'PAnchmahal(Godhara)',
  //   'Gandhinagar',
  //   'bardoli',
  //   'Dahod',
  //   'Navsari',
  //   'Narmada',
  //   'Anand',
  //   'Patan',
  //   'Porbandar',
  //   'Vyara',
  //   'Gandhidham',
  //   'Botad',
  //   'Modasa',
  //   'Dwarka',
  //   'Morbi'
  // ];

  
  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TravelpackagePage');
  }
  itemSelected(item: string) {
    console.log("Selected Item", item);
  }
}
