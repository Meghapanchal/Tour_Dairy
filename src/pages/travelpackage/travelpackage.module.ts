import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TravelpackagePage } from './travelpackage';

@NgModule({
  declarations: [
    TravelpackagePage,
  ],
  imports: [
    IonicPageModule.forChild(TravelpackagePage),
  ],
})
export class TravelpackagePageModule {}
