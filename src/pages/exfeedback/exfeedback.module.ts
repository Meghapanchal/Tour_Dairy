import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ExfeedbackPage } from './exfeedback';

@NgModule({
  declarations: [
    ExfeedbackPage,
  ],
  imports: [
    IonicPageModule.forChild(ExfeedbackPage),
  ],
})
export class ExfeedbackPageModule {}
