import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TopdestinationPage } from './topdestination';

@NgModule({
  declarations: [
    TopdestinationPage,
  ],
  imports: [
    IonicPageModule.forChild(TopdestinationPage),
  ],
})
export class TopdestinationPageModule {}
