import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FooddrinkPage } from './fooddrink';

@NgModule({
  declarations: [
    FooddrinkPage,
  ],
  imports: [
    IonicPageModule.forChild(FooddrinkPage),
  ],
})
export class FooddrinkPageModule {}
