import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { FlightSearchResultPage } from './flight-search-result';

@NgModule({
  declarations: [
    FlightSearchResultPage,
  ],
  imports: [
    IonicPageModule.forChild(FlightSearchResultPage),
  ],
})
export class FlightSearchResultPageModule {}
