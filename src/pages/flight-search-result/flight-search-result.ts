import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Platform } from 'ionic-angular';
import { AlertController, PopoverController, Popover , App, FabContainer, ItemSliding, List, ModalController, ToastController, LoadingController, Refresher } from 'ionic-angular';
import { DataBaseProvider } from "../../providers/data-base/data-base";
import { HomePage } from '../home/home';
import * as firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-flight-search-result',
  templateUrl: 'flight-search-result.html',
})
export class FlightSearchResultPage {
  userFeeds: any;
  price1 = 3120;
  price2 = 2500;
  price3 = 3600;
  price4 = 2910;

  // uniqkey: any;
  // flight: any;
  // public currentUser: firebase.User;
  // profileData: any;
  // public userProfile: firebase.database.Reference;


  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public alertCtrl: AlertController,
    public app: App,
    public loadingCtrl: LoadingController,
    public modalCtrl: ModalController,
    public toastCtrl: ToastController,
    // public confData: ConferenceData,
    public popoverCtrl: PopoverController,
    public platform: Platform,
    private DB: DataBaseProvider) {
      
      // this.flight = this.navParams.get('flght');

    }

  ionViewDidEnter() {
    this.platform.ready()
   .then(()=>{
     this.loadFeeds();
   });
 }

  ionViewDidLoad() {
    console.log('ionViewDidLoad FlightSearchResultPage');
  }
  loadFeeds() {
    this.userFeeds = this.DB.renderFeeds();
    console.log(this.DB.renderFeeds());
  }

  presentPopover(event: Event) {
    let popover = this.popoverCtrl.create(HomePage);
    popover.present({ ev: event });
  }

  // book(from, to, departure, person, economyclass) {
  //   firebase.auth().onAuthStateChanged(user => {
  //     if (user) {
  //       this.currentUser = user;
  //       this.userProfile = firebase.database().ref(`/userProfile/${user.uid}`);
  //       this.userProfile.on('value', userProfileData => {
  //         this.profileData = userProfileData.val();
  //         this.uniqkey = Math.floor(Math.random() * 1000000);
  //        firebase.database().ref('/book').child(this.uniqkey).set({ From: from , To:to, Departure:departure, Person:person, Economyclass:economyclass , user: this.profileData  });
  //       });
  //     }
  //   }); 
  // }

}
