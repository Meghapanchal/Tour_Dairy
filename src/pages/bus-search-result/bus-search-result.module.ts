import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BusSearchResultPage } from './bus-search-result';

@NgModule({
  declarations: [
    BusSearchResultPage,
  ],
  imports: [
    IonicPageModule.forChild(BusSearchResultPage),
  ],
})
export class BusSearchResultPageModule {}
