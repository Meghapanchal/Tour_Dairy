import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OnewayPage } from './oneway';

@NgModule({
  declarations: [
    OnewayPage,
  ],
  imports: [
    IonicPageModule.forChild(OnewayPage),
  ],
})
export class OnewayPageModule {}
