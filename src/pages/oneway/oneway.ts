import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';


import { RoundtripPage } from '../roundtrip/roundtrip';
import { MulticityPage } from '../multicity/multicity';

@IonicPage()
@Component({
  selector: 'page-oneway',
  templateUrl: 'oneway.html',
})
export class OnewayPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OnewayPage');
  }
  oneway(){
    this.navCtrl.setRoot(OnewayPage);
  }
  roundtrip(){
    this.navCtrl.setRoot(RoundtripPage);
  }

  multicity(){
    this.navCtrl.setRoot(MulticityPage);
  }
}
