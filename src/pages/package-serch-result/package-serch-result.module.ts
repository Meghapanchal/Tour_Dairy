import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PackageSerchResultPage } from './package-serch-result';

@NgModule({
  declarations: [
    PackageSerchResultPage,
  ],
  imports: [
    IonicPageModule.forChild(PackageSerchResultPage),
  ],
})
export class PackageSerchResultPageModule {}
