import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { GettingaroundsPage } from './gettingarounds';

@NgModule({
  declarations: [
    GettingaroundsPage,
  ],
  imports: [
    IonicPageModule.forChild(GettingaroundsPage),
  ],
})
export class GettingaroundsPageModule {}
