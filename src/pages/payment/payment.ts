import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams} from 'ionic-angular';
//,Loading,LoadingController,AlertController 
// import { FormBuilder, Validators, FormGroup } from '@angular/forms';
// import firebase from 'firebase';
// import { ValidatorPage } from '../validator/validator';
// import { AuthProvider } from "../../providers/auth/auth";
@IonicPage()
@Component({
  selector: 'page-payment',
  templateUrl: 'payment.html',
})
export class PaymentPage {
  // public paymentForm: FormGroup;
  // public loading: Loading;
  

  constructor(public navCtrl: NavController, public navParams: NavParams
    // public formBuilder: FormBuilder, 
    // public loadingCtrl: LoadingController,
    // public authProvider: AuthProvider, 
    // public alertCtrl: AlertController
  ) {
    // this.paymentForm = formBuilder.group({
    //   cname:['', Validators.compose([Validators.minLength(2), Validators.required])],
    //  // number:['', Validators.compose([Validators.maxLength(12), Validators.required])],
    //   cnumber: ['',Validators.compose([Validators.maxLength(12), Validators.required])],
    //   cvvnumber:['',Validators.compose([Validators.maxLength(3), Validators.required])]
    // })
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PaymentPage');
  }
  // paynow():void{
    
  //   if (this.paymentForm.value.cnumber !== this.paymentForm.value.cnumber) {
  //     let alert = this.alertCtrl.create({
  //       title: 'Error',
  //       message: 'Your card number and cvv number wrong.',
  //       buttons: ['OK']
  //     });
  //     alert.present();
  //     return;
  //   }

  //   if (!this.paymentForm.valid){
  //     console.log(this.paymentForm.value);
  //   } else {
  //     this.authProvider.paynow(this.paymentForm.value.cname, this.paymentForm.value.cvvnumber)
  //     .then(() => {
  //       this.loading.dismiss().then( () => {
  //         this.navCtrl.setRoot(PaymentPage);
  //       });
  //     }, (error) => {
  //       this.loading.dismiss().then( () => {
  //         let alert = this.alertCtrl.create({
  //           message: error.message,
  //           buttons: [
  //             {
  //               text: "Ok",
  //               role: 'cancel'
  //             }
  //           ]
  //         });
  //         alert.present();
  //       });
  //     });
  //     this.loading = this.loadingCtrl.create();
  //     this.loading.present();
  //   }
  // }
}
