import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { RoundtripcabPage } from '../roundtripcab/roundtripcab';

@IonicPage()
@Component({
  selector: 'page-onewaycab',
  templateUrl: 'onewaycab.html',
})
export class OnewaycabPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad OnewaycabPage');
  }
  oneway(){
    this.navCtrl.setRoot(OnewaycabPage);
  }

  roundtrip(){
    this.navCtrl.setRoot(RoundtripcabPage);
  }
}
