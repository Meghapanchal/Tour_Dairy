import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { OnewaycabPage } from './onewaycab';

@NgModule({
  declarations: [
    OnewaycabPage,
  ],
  imports: [
    IonicPageModule.forChild(OnewaycabPage),
  ],
})
export class OnewaycabPageModule {}
