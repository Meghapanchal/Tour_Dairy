import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { RestApiProvider } from "../../providers/rest-api/rest-api";
import 'rxjs/Rx';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { HotelSearchResultPage } from "../hotel-search-result/hotel-search-result";

@IonicPage()
@Component({
  selector: 'page-hotel',
  templateUrl: 'hotel.html',
})
export class HotelPage {

 
  hotel: any;
  public searchHotel: FormGroup;

  public event = {
    month: '2018-01-1',
    timeStarts: '07:43',
    timeEnds: '2020-01-01'
  }


  constructor(public navCtrl: NavController, public navParams: NavParams,  public api: RestApiProvider, public formBuilder: FormBuilder) {
    this.searchHotel = formBuilder.group({
      EnterCity: ['', Validators.compose([Validators.required])],
      EnterYourBudget: ['', Validators.compose([Validators.required])],
      Rate: ['', Validators.compose([Validators.required])],
      Checkindate: ['', Validators.compose([Validators.required])],
      Checkoutdate: ['', Validators.compose([Validators.required])],
      Rooms: ['', Validators.compose([Validators.required])],
      // Person: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HotelPage');
    this.api.getHotel().then(data =>{
      this.hotel = data;
      console.log("data : ",this.hotel.json().hotel1)
    })
  }
  search() {
    console.log("Search");
    this.api.getHotel().then(data =>{
      this.hotel = data;
      if(this.searchHotel.value.from == this.hotel.json().hotel1.from && this.searchHotel.value.to == this.hotel.json().hotel1.to) {
        console.log("success !!!");
        this.navCtrl.push(HotelSearchResultPage);
      }
      else {
        console.log("no data found ... ")
      }
    });
  }
}
