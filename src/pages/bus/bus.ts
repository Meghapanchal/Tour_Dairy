import { Component } from '@angular/core';
import { NavController, NavParams} from 'ionic-angular';
import { RestApiProvider } from "../../providers/rest-api/rest-api";

import 'rxjs/Rx';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { BusSearchResultPage } from "../bus-search-result/bus-search-result";

// @IonicPage()
@Component({
  selector: 'page-bus',
  templateUrl: 'bus.html',
})
export class BusPage {

  bus: any;
  public searchBus: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public api: RestApiProvider, public formBuilder: FormBuilder) {
    this.searchBus = formBuilder.group({
      from: ['', Validators.compose([Validators.required])],
      to: ['', Validators.compose([Validators.required])],
      departure: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad BusPage');
    this.api.getBus().then(data =>{
      this.bus = data;
      console.log("data : ",this.bus.json().bus1)
    })
  }
  search() {
    console.log("Search");
    this.api.getBus().then(data =>{
      this.bus = data;
      if(this.searchBus.value.from == this.bus.json().bus1.from && this.searchBus.value.to == this.bus.json().bus1.to) {
        console.log("success !!!");
        this.navCtrl.push(BusSearchResultPage);
      }
      else {
        console.log("no data found ... ")
      }
    });
  }
}
