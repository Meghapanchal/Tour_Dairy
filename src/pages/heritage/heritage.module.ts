import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HeritagePage } from './heritage';

@NgModule({
  declarations: [
    HeritagePage,
  ],
  imports: [
    IonicPageModule.forChild(HeritagePage),
  ],
})
export class HeritagePageModule {}
