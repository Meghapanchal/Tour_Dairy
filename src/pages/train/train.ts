import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { RestApiProvider } from "../../providers/rest-api/rest-api";
import 'rxjs/Rx';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { TrainSearchResultPage } from "../train-search-result/train-search-result";

@IonicPage()
@Component({
  selector: 'page-train',
  templateUrl: 'train.html',
})
export class TrainPage {

  train: any;
  public searchTrain: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public api: RestApiProvider, public formBuilder: FormBuilder) {
    this.searchTrain = formBuilder.group({
      from: ['', Validators.compose([Validators.required])],
      to: ['', Validators.compose([Validators.required])],
      departure: ['', Validators.compose([Validators.required])],
      person: ['', Validators.compose([Validators.required])],
      class: ['', Validators.compose([Validators.required])]
    });
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TrainPage');
     this.api.getTrain().then(data =>{
      this.train = data;
      console.log("data : ",this.train.json().train1)
    })
  }
  search() {
    console.log("Search");
    this.api.getTrain().then(data =>{
      this.train = data;
      if(this.searchTrain.value.from == this.train.json().train1.from && this.searchTrain.value.to == this.train.json().train1.to) {
        console.log("success !!!");
        this.navCtrl.push(TrainSearchResultPage);
      }
      else {
        console.log("no data found ... ")
      }
    });
  }
}
