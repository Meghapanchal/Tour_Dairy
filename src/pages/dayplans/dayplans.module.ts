import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { DayplansPage } from './dayplans';

@NgModule({
  declarations: [
    DayplansPage,
  ],
  imports: [
    IonicPageModule.forChild(DayplansPage),
  ],
})
export class DayplansPageModule {}
