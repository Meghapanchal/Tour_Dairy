import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { OnewayPage } from '../oneway/oneway';
import { MulticityPage } from '../multicity/multicity';


@IonicPage()
@Component({
  selector: 'page-roundtrip',
  templateUrl: 'roundtrip.html',
})
export class RoundtripPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad RoundtripPage');
  }


  oneway(){
    this.navCtrl.setRoot(OnewayPage);
  }
  roundtrip(){
    this.navCtrl.setRoot(RoundtripPage);
  }
  multicity(){
    this.navCtrl.setRoot(MulticityPage);
  }


}
