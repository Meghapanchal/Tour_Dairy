import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { RoundtripPage } from './roundtrip';

@NgModule({
  declarations: [
    RoundtripPage,
  ],
  imports: [
    IonicPageModule.forChild(RoundtripPage),
  ],
})
export class RoundtripPageModule {}
