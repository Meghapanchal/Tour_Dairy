import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, AlertController } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { LoginPage } from '../login/login';
import * as firebase from 'firebase';
import { AngularFireAuth } from 'angularfire2/auth';

@IonicPage()
@Component({
  selector: 'page-profile',
  templateUrl: 'profile.html',
})
export class ProfilePage {

  public currentUser: firebase.User;
  profileData: any;
  public userProfile: firebase.database.Reference;

  constructor(public alertCtrl: AlertController, public navCtrl: NavController, public navParams: NavParams, public af: AngularFireDatabase, private afAuth: AngularFireAuth) {
    // this.profileData = af.list('/userProfile');
  }

  ionViewDidLoad() {
    this.profile();
  }

  profile() {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.currentUser = user;
        this.userProfile = firebase.database().ref(`/userProfile/${user.uid}`);
        this.userProfile.on('value', userProfileData => {
          this.profileData = userProfileData.val();
        });
      }
    });
  }
  
  logOut() {
    this.afAuth.auth.signOut();
    this.navCtrl.setRoot(LoginPage);
  }
}
