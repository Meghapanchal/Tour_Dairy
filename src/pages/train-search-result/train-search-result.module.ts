import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TrainSearchResultPage } from './train-search-result';

@NgModule({
  declarations: [
    TrainSearchResultPage,
  ],
  imports: [
    IonicPageModule.forChild(TrainSearchResultPage),
  ],
})
export class TrainSearchResultPageModule {}
