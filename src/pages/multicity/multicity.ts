import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { OnewayPage } from '../oneway/oneway';
import { RoundtripPage } from '../roundtrip/roundtrip';

@IonicPage()
@Component({
  selector: 'page-multicity',
  templateUrl: 'multicity.html',
})
export class MulticityPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad MulticityPage');
  }
  oneway(){
    this.navCtrl.setRoot(OnewayPage);
  }

  roundtrip(){
    this.navCtrl.setRoot(RoundtripPage);
  }
  multicity(){
    this.navCtrl.setRoot(MulticityPage);
  }
}
