import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { MulticityPage } from './multicity';

@NgModule({
  declarations: [
    MulticityPage,
  ],
  imports: [
    IonicPageModule.forChild(MulticityPage),
  ],
})
export class MulticityPageModule {}
