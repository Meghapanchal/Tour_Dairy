import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController } from 'ionic-angular';
import * as firebase from 'firebase';

@IonicPage()
@Component({
  selector: 'page-packagedetails',
  templateUrl: 'packagedetails.html',
})
export class PackagedetailsPage {

  //
  uniqkey: any;
  package: any;
  public currentUser: firebase.User;
  profileData: any;
  public userProfile: firebase.database.Reference;

  constructor(public navCtrl: NavController, 
    public navParams: NavParams,
    public toastCtrl: ToastController) {
    this.package = this.navParams.get('pkg');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PackagedetailsPage');
  }

  // showToastWithCloseButton() {
  //   const toast = this.toastCtrl.create({
  //     message: 'Your files were successfully saved',
  //     showCloseButton: true,
  //     closeButtonText: 'Ok'
  //   });
  //   toast.present();
  // }

  book(date, person, stay, visiting, details) {
    firebase.auth().onAuthStateChanged(user => {
      if (user) {
        this.currentUser = user;
        this.userProfile = firebase.database().ref(`/userProfile/${user.uid}`);
        this.userProfile.on('value', userProfileData => {
          this.profileData = userProfileData.val();
          this.uniqkey = Math.floor(Math.random() * 1000000);
         firebase.database().ref('/book').child(this.uniqkey).set({ Date: date, person: person, stay: stay, visiting: visiting, details:details , user: this.profileData  });
        });
      }
    });
  }

}
