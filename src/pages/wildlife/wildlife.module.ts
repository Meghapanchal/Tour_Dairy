import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { WildlifePage } from './wildlife';

@NgModule({
  declarations: [
    WildlifePage,
  ],
  imports: [
    IonicPageModule.forChild(WildlifePage),
  ],
})
export class WildlifePageModule {}
