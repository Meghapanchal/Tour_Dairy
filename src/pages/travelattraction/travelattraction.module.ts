import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { TravelattractionPage } from './travelattraction';

@NgModule({
  declarations: [
    TravelattractionPage,
  ],
  imports: [
    IonicPageModule.forChild(TravelattractionPage),
  ],
})
export class TravelattractionPageModule {}
