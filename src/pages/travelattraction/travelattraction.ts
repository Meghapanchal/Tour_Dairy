import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { WildlifePage } from '../wildlife/wildlife';
import { BeachesPage } from '../beaches/beaches';
import { YogaPage } from '../yoga/yoga';
import { HeritagePage } from '../heritage/heritage';
import { HillstationPage } from '../hillstation/hillstation';
import { HoneymoonPage } from '../honeymoon/honeymoon';

@IonicPage()
@Component({
  selector: 'page-travelattraction',
  templateUrl: 'travelattraction.html',
})
export class TravelattractionPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TravelattractionPage');
  }
  wildlife(){
    this.navCtrl.setRoot(WildlifePage);
  }
  hillstation(){
    this.navCtrl.setRoot(HillstationPage);
  }
  beaches(){
    this.navCtrl.setRoot(BeachesPage);
  }
  yoga(){
    this.navCtrl.setRoot(YogaPage);
  }
  her(){
    this.navCtrl.setRoot(HeritagePage);
  }
  honey(){
    this.navCtrl.setRoot(HoneymoonPage);
  }
}
