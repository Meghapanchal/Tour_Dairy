import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ShowpackagePage } from './showpackage';

@NgModule({
  declarations: [
    ShowpackagePage,
  ],
  imports: [
    IonicPageModule.forChild(ShowpackagePage),
  ],
})
export class ShowpackagePageModule {}
