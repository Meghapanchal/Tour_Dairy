import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CardholderPage } from './cardholder';

@NgModule({
  declarations: [
    CardholderPage,
  ],
  imports: [
    IonicPageModule.forChild(CardholderPage),
  ],
})
export class CardholderPageModule {}
