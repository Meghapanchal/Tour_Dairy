import { Component, ViewChild } from '@angular/core';
import { NavController, Slides } from 'ionic-angular';

import { SingupPage } from '../singup/singup';
import { BusPage } from '../Bus/Bus';
import { FlightsPage } from '../flights/flights';
import { HotelPage } from '../hotel/hotel';
import { HolidayPage } from '../holiday/holiday';
import { TrainPage } from '../train/train';
import { CabPage } from '../cab/cab';
import { TravelattractionPage } from '../travelattraction/travelattraction';
import { TopdestinationPage } from '../topdestination/topdestination';
import { TravelpackagePage } from '../travelpackage/travelpackage';
import { TourdiaryspecialPage } from '../tourdiaryspecial/tourdiaryspecial';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  
  @ViewChild(Slides) slides: Slides;

  constructor(public navCtrl: NavController) {

  }
  
  ionViewDidLoad() {
    console.log('ionViewDidLoad HomePage');
  }
    signin(){
      this.navCtrl.setRoot(SingupPage);
    }

    hotel(){
      this.navCtrl.setRoot(HotelPage);
    }
    flight(){
      this.navCtrl.setRoot(FlightsPage);
    }
    holiday(){
      this.navCtrl.setRoot(HolidayPage);
    }
    bus(){
      this.navCtrl.setRoot(BusPage);
    }
    train(){
      this.navCtrl.setRoot(TrainPage);
    }
    cab(){
      this.navCtrl.setRoot(CabPage);
    }
  goToSlide() {
    this.slides.slideTo(2, 500);    
  }
  ta(){
    this.navCtrl.setRoot(TravelattractionPage);
  }
  tp(){
    this.navCtrl.setRoot(TravelpackagePage);
  }
  td(){
    this.navCtrl.setRoot(TopdestinationPage);
  }
  tu(){
    this.navCtrl.setRoot(TourdiaryspecialPage);
}
 
}


