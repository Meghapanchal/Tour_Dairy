import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Loading,LoadingController,AlertController } from 'ionic-angular';


import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import firebase from 'firebase';
import { ValidatorPage } from '../validator/validator';
import { AuthProvider } from "../../providers/auth/auth";
import { HomePage } from '../home/home';
@IonicPage()
@Component({
  selector: 'page-singup',
  templateUrl: 'singup.html',
})
export class SingupPage {
  public signupForm: FormGroup;
  public loading:Loading;
  constructor(public navCtrl: NavController, public navParams: NavParams, 
    public formBuilder: FormBuilder, 
    public loadingCtrl: LoadingController,
    public authProvider: AuthProvider, 
    public alertCtrl: AlertController
  ) {
    this.signupForm = formBuilder.group({
      fname: ['', Validators.compose([Validators.minLength(2), Validators.required])],
      lname: ['', Validators.compose([Validators.minLength(2), Validators.required])],
      email: ['', Validators.compose([Validators.required, ValidatorPage.isValid])],
      password: ['', Validators.compose([Validators.minLength(6), Validators.required])],
      passwordRetyped: ['', Validators.compose([Validators.minLength(6), Validators.required])]

    });
  }


  ionViewDidLoad() {
    console.log('ionViewDidLoad SingupPage');
  }
  signup():void{
    
    if (this.signupForm.value.password !== this.signupForm.value.passwordRetyped) {
      let alert = this.alertCtrl.create({
        title: 'Error',
        message: 'Your password and your re-entered password does not match each other.',
        buttons: ['OK']
      });
      alert.present();
      return;
    }

    if (!this.signupForm.valid){
      console.log(this.signupForm.value);
    } else {
      this.authProvider.signup(this.signupForm.value.email, this.signupForm.value.password, this.signupForm.value.fname, this.signupForm.value.lname)
      .then(() => {
        this.loading.dismiss().then( () => {
          this.navCtrl.setRoot(HomePage);
        });
      }, (error) => {
        this.loading.dismiss().then( () => {
          let alert = this.alertCtrl.create({
            message: error.message,
            buttons: [
              {
                text: "Ok",
                role: 'cancel'
              }
            ]
          });
          alert.present();
        });
      });
      this.loading = this.loadingCtrl.create();
      this.loading.present();
    }
  }

  }


