import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { SavedplacesPage } from './savedplaces';

@NgModule({
  declarations: [
    SavedplacesPage,
  ],
  imports: [
    IonicPageModule.forChild(SavedplacesPage),
  ],
})
export class SavedplacesPageModule {}
