import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { AngularFireDatabase, FirebaseListObservable } from 'angularfire2/database-deprecated';
import { PackagedetailsPage } from '../packagedetails/packagedetails';

@IonicPage()
@Component({
  selector: 'page-package',
  templateUrl: 'package.html',
})
export class PackagePage {

  packages: FirebaseListObservable<any[]>;

  constructor(public navCtrl: NavController, public navParams: NavParams, public af: AngularFireDatabase) {
    this.packages = af.list('/tour');
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PackagePage');
  }

  packageDetails(pkg) {
    this.navCtrl.push(PackagedetailsPage, {pkg: pkg});
  }

}
