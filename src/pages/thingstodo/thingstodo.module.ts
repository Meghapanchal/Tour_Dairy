import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ThingstodoPage } from './thingstodo';

@NgModule({
  declarations: [
    ThingstodoPage,
  ],
  imports: [
    IonicPageModule.forChild(ThingstodoPage),
  ],
})
export class ThingstodoPageModule {}
