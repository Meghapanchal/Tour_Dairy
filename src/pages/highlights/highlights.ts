import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { FooddrinkPage } from '../foodanddrinks/fooddrink';
import { GettingaroundsPage } from '../getting around/gettingarounds';
import { ReservationPage } from '../reservation/reservation'; 
import { SavedplacesPage } from '../Saved places/savedplaces';
import { DayplansPage } from '../dayplans/dayplans';
import { ThingstodoPage } from '../thingstodo/thingstodo';

@IonicPage()
@Component({
  selector: 'page-highlights',
  templateUrl: 'highlights.html',
})
export class HighlightsPage {

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad HighlightsPage');
  }
  sp(){
    this.navCtrl.setRoot(SavedplacesPage);
  }
  td(){
    this.navCtrl.setRoot(ThingstodoPage);
  }
  rs(){
    this.navCtrl.setRoot(ReservationPage);
  }
  dp(){
    this.navCtrl.setRoot(DayplansPage);
  }
  fd(){
    this.navCtrl.setRoot(FooddrinkPage);
  }
  ga(){
    this.navCtrl.setRoot(GettingaroundsPage);
  }
}
