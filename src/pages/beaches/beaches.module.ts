import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { BeachesPage } from './beaches';

@NgModule({
  declarations: [
    BeachesPage,
  ],
  imports: [
    IonicPageModule.forChild(BeachesPage),
  ],
})
export class BeachesPageModule {}
