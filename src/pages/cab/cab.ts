import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { RestApiProvider } from "../../providers/rest-api/rest-api";
import { OnewaycabPage } from '../onewaycab/onewaycab';
import { RoundtripcabPage } from '../roundtripcab/roundtripcab';
import { CabSearchResultPage } from '../cab-search-result/cab-search-result';

import 'rxjs/Rx';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@IonicPage()
@Component({
  selector: 'page-cab',
  templateUrl: 'cab.html',
})
export class CabPage {

    cab:any;
    public searchCab: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams, public api: RestApiProvider, public formBuilder: FormBuilder) {
    this.searchCab = formBuilder.group({
      from: ['', Validators.compose([Validators.required])],
      to: ['', Validators.compose([Validators.required])],
      departure: ['', Validators.compose([Validators.required])],
      Return: ['', Validators.compose([Validators.required])],
      PickupTime : ['', Validators.compose([Validators.required])]
    });
 
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad CabPage');
    this.api.getCab().then(data =>{
      this.cab = data;
      console.log("data : ",this.cab.json().cab1)
    })
  }

  oneway(){
    this.navCtrl.setRoot(OnewaycabPage);
  }

  roundtrip(){
    this.navCtrl.setRoot(RoundtripcabPage);
  }
  search() {
    console.log("Search");
    this.api.getCab().then(data =>{
      this.cab = data;
      if(this.searchCab.value.from == this.cab.json().cab1.from && this.searchCab.value.to == this.cab.json().cab1.to) {
        console.log("success !!!");
        this.navCtrl.push(CabSearchResultPage);
      }
      else {
        console.log("no data found ... ")
      }
    });
  }
}
