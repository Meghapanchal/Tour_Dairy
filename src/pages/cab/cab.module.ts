import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { CabPage } from './cab';

@NgModule({
  declarations: [
    CabPage,
  ],
  imports: [
    IonicPageModule.forChild(CabPage),
  ],
})
export class CabPageModule {}
