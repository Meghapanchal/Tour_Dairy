import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { PackrequPage } from './packrequ';

@NgModule({
  declarations: [
    PackrequPage,
  ],
  imports: [
    IonicPageModule.forChild(PackrequPage),
  ],
})
export class PackrequPageModule {}
