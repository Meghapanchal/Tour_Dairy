import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';
import { RestApiProvider } from "../../providers/rest-api/rest-api";
import 'rxjs/Rx';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { PackageSerchResultPage } from '../package-serch-result/package-serch-result';


@IonicPage()
@Component({
  selector: 'page-packrequ',
  templateUrl: 'packrequ.html',
})
export class PackrequPage {

  package: any;
  public searchPackage: FormGroup;

  constructor(public navCtrl: NavController, public navParams: NavParams,public api: RestApiProvider, public formBuilder: FormBuilder) {
  
    this.searchPackage = formBuilder.group({
      visitingPlace: ['', Validators.compose([Validators.required])],
      stay: ['', Validators.compose([Validators.required])],
      arrival: ['', Validators.compose([Validators.required])],
      datetime: ['', Validators.compose([Validators.required])],
      person: ['', Validators.compose([Validators.required])],
      requirements: ['', Validators.compose([Validators.required])],
      // Person: ['', Validators.compose([Validators.required])]
    });

  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad PackrequPage');
    this.api.getPackage().then(data =>{
      this.package = data;
      console.log("data : ",this.package.json().package1)
    })
  }
  search() {
    console.log("Search");
    this.api.getPackage().then(data =>{
      this.package = data;
      if(this.searchPackage.value.from == this.package.json().package1.from && this.searchPackage.value.to == this.package.json().package1.to) {
        console.log("success !!!");
        this.navCtrl.push(PackageSerchResultPage);
      }
      else {
        console.log("no data found ... ")
      }
    });
  }
}
