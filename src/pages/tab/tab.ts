import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams } from 'ionic-angular';

import { HomePage } from '../home/home';
//import { SettingPage } from '../setting/setting';
import { HighlightsPage } from '../highlights/highlights';
import { HolidayPage } from '../holiday/holiday';
@IonicPage()

@Component({
  selector: 'page-tab',
  templateUrl: 'tab.html',
})
export class TabPage {


  tab1Root = HomePage;
  tab2Root = HighlightsPage;
  tab4Root = HolidayPage;
  

  constructor(public navCtrl: NavController, public navParams: NavParams) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad TabPage');
  }

}
