import { Http } from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/add/operator/map';
import { Observable } from "rxjs/Observable";
import * as firebase from 'firebase';


@Injectable()
export class DataBaseProvider {

  constructor(public http: Http) {
    console.log('Hello DataBaseProvider Provider');
  }
  renderFeeds(): Observable<any>{
    try {
      return new Observable(observer => {
        let feed: any = [];

        firebase.database().ref('moderator/').orderByKey().once('value', (items:any) =>{
          observer.next(feed);
          observer.complete();

          items.forEach(item => {
            feed.push(item.val());
          });

           observer.next(feed.item);
          observer.complete();

         })
      })
    } catch (error) {
      
    }
  }

  renderStory(): Observable<any>{
    try {
      return new Observable(observer => {
        let story: any = [];

        firebase.database().ref('Moderator_Story/').orderByKey().once('value', (items:any) =>{
          observer.next(story);
          observer.complete();

          items.forEach(item =>{
            story.push(item.val());
          });

          observer.next(story.item);
          observer.complete();

        })
      })
    } catch (error) {
      
    }
  }

  userProfile(): Observable<any>{
    try {
      return new Observable(observer => {
        let userprofile: any = [];

        firebase.database().ref('userProfile/').orderByKey().once('value', (items:any) =>{
          observer.next(userprofile);
          observer.complete();

           items.forEach(item =>{
            userprofile.push(item.val());
          });

           observer.next(userprofile.item);
          observer.complete();

        })

      })
    } catch (error) {
      
    }
  }
}
