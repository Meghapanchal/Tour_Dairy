import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/Rx';

@Injectable()
export class RestApiProvider {

  data: any;
  constructor(public http: Http) {
    console.log('Hello RestApiProvider Provider');
  }

  apiUrlflight = 'assets/api/flight.json';

  getFlight() {
    return new Promise(resolve => {
      this.http.get(this.apiUrlflight).subscribe(data => {
        resolve(data);
        console.log(data.json())
      }, err => {
        console.log(err);
      });
    });
  }
  apiUrlcab = 'assets/api/cab.json';

  getCab() {
    return new Promise(resolve => {
      this.http.get(this.apiUrlcab).subscribe(data => {
        resolve(data);
        console.log(data.json())
      }, err => {
        console.log(err);
      });
    });
  }
  apiUrltrain = 'assets/api/train.json';

  getTrain() {
    return new Promise(resolve => {
      this.http.get(this.apiUrltrain).subscribe(data => {
        resolve(data);
        console.log(data.json())
      }, err => {
        console.log(err);
      });
    });
  }
  apiUrlbus = 'assets/api/bus.json';

  getBus() {
    return new Promise(resolve => {
      this.http.get(this.apiUrlbus).subscribe(data => {
        resolve(data);
        console.log(data.json())
      }, err => {
        console.log(err);
      });
    });
  }
  apiUrlhotel = 'assets/api/hotel.json';

  getHotel() {
    return new Promise(resolve => {
      this.http.get(this.apiUrlhotel).subscribe(data => {
        resolve(data);
        console.log(data.json())
      }, err => {
        console.log(err);
      });
    });
  }

  apiUrlpackage = 'assets/api/tour.json';

  getPackage() {
    return new Promise(resolve => {
      this.http.get(this.apiUrlpackage).subscribe(data => {
        resolve(data);
        console.log(data.json())
      }, err => {
        console.log(err);
      });
    });
  }
  // getFlight() {
  //   return new Promise(resolve => {
  //     this.http.get(this.apiUrl)
  //       .map(res => res.json()).subscribe(data => {
  //         this.data = data;
  //         console.log(data);
  //       })
  //   })
  // }
}
