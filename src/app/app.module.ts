import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { MyApp } from './app.component';
import { HomePage } from '../pages/home/home';
import { TabPage } from '../pages/tab/tab';
import { AboutPage } from '../pages/about/about';
import { SingupPage } from '../pages/singup/singup';
import { AuthProvider } from '../providers/auth/auth';
import { LoginPage } from '../pages/login/login';
import { BusPage } from '../pages/Bus/Bus';
import { FlightsPage } from '../pages/flights/flights';
import { HotelPage } from '../pages/hotel/hotel';
import { HolidayPage } from '../pages/holiday/holiday';
import { TrainPage } from '../pages/train/train';
import { CabPage } from '../pages/cab/cab';
import { OnewayPage } from '../pages/oneway/oneway';
import { RoundtripPage } from '../pages/roundtrip/roundtrip';
import { MulticityPage } from '../pages/multicity/multicity';
import { OnewaycabPage } from '../pages/onewaycab/onewaycab';
import { RoundtripcabPage } from '../pages/roundtripcab/roundtripcab';
import { ExfeedbackPage } from '../pages/exfeedback/exfeedback';
import { PasswordResetPage } from "../pages/password-reset/password-reset";
import { ValidatorPage } from '../pages/validator/validator';
import { HighlightsPage } from '../pages/highlights/highlights';
import { TravelattractionPage } from '../pages/travelattraction/travelattraction';
import { TopdestinationPage } from '../pages/topdestination/topdestination';
import { TravelpackagePage } from '../pages/travelpackage/travelpackage';
import { TourdiaryspecialPage } from '../pages/tourdiaryspecial/tourdiaryspecial';
import { FooddrinkPage } from '../pages/foodanddrinks/fooddrink';
import { GettingaroundsPage } from '../pages/getting around/gettingarounds';
import { ReservationPage } from '../pages/reservation/reservation'; 
import { SavedplacesPage } from '../pages/Saved places/savedplaces';
import { DayplansPage } from '../pages/dayplans/dayplans';
import { ThingstodoPage } from '../pages/thingstodo/thingstodo';
import { PackrequPage } from '../pages/packrequ/packrequ';
import { ProfilePage } from '../pages/profile/profile';
import { AngularFireModule } from 'angularfire2';
import { AngularFireDatabaseModule } from 'angularfire2/database';
import { AngularFireAuthModule } from 'angularfire2/auth';
import { IonicImageViewerModule } from 'ionic-img-viewer';
import firebase from 'firebase';
import { RestApiProvider } from '../providers/rest-api/rest-api';
import { HttpModule } from '@angular/http';
import { FlightSearchResultPage } from "../pages/flight-search-result/flight-search-result";
import { CabSearchResultPage } from "../pages/cab-search-result/cab-search-result";
import { BusSearchResultPage } from "../pages/bus-search-result/bus-search-result";
import { TrainSearchResultPage } from "../pages/train-search-result/train-search-result";
import { HotelSearchResultPage } from "../pages/hotel-search-result/hotel-search-result";
import { DataBaseProvider } from '../providers/data-base/data-base';
import { Camera } from '@ionic-native/camera';
import { ShowpackagePage } from '../pages/showpackage/showpackage';
import { AngularFireDatabase } from 'angularfire2/database-deprecated';
import { WildlifePage } from '../pages/wildlife/wildlife';
import { BeachesPage } from '../pages/beaches/beaches';
import { HeritagePage } from '../pages/heritage/heritage';
import { HoneymoonPage } from '../pages/honeymoon/honeymoon';
import { HillstationPage } from '../pages/hillstation/hillstation';
import { PaymentPage } from '../pages/payment/payment';
import { CardholderPage } from '../pages/cardholder/cardholder';
import { YogaPage } from '../pages/yoga/yoga';
import { PackageSerchResultPage } from '../pages/package-serch-result/package-serch-result';
import { PackagePage } from '../pages/package/package';
import { PackagedetailsPage } from '../pages/packagedetails/packagedetails';

var config = {
  apiKey: "AIzaSyAVJ_FH-4vjBKlyUEDGG4cTPWhoyMJD3Do",
  authDomain: "tour-diary123.firebaseapp.com",
  databaseURL: "https://tour-diary123.firebaseio.com",
  projectId: "tour-diary123",
  storageBucket: "tour-diary123.appspot.com",
  messagingSenderId: "1023244056776"
};


@NgModule({
  declarations: [
    MyApp,
    HomePage,
    TabPage,
    AboutPage,
    SingupPage,
    LoginPage,
    FlightsPage,
    HotelPage,
    BusPage,
    HolidayPage,
    TrainPage,
    CabPage,
    OnewayPage,
    RoundtripPage,
    MulticityPage,
    OnewaycabPage,
    RoundtripcabPage,
    ExfeedbackPage,
    ValidatorPage,
    PasswordResetPage,
    HighlightsPage,
    TravelattractionPage,
    TravelpackagePage,
    TopdestinationPage,
    TourdiaryspecialPage,
    ThingstodoPage,
    ReservationPage,
    SavedplacesPage,
    FooddrinkPage,
    DayplansPage,
    GettingaroundsPage,
    PackrequPage,
    PasswordResetPage,
    ProfilePage,
    FlightSearchResultPage,
    CabSearchResultPage,
    HotelSearchResultPage,
    BusSearchResultPage,
    TrainSearchResultPage,
    ShowpackagePage,
    HillstationPage,
    HoneymoonPage,
    YogaPage,
    WildlifePage,
    BeachesPage,
    HeritagePage,
    PaymentPage,
    PackagePage,
    CardholderPage,
    PackageSerchResultPage,
    PackagedetailsPage
  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    IonicImageViewerModule,
    AngularFireModule.initializeApp(config),
    AngularFireDatabaseModule,
    AngularFireAuthModule,
    HttpModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    HomePage,
    TabPage,
    AboutPage,
    SingupPage,
    LoginPage,
    FlightsPage,
    HotelPage,
    BusPage,
    HolidayPage,
    TrainPage,
    PasswordResetPage,
    CabPage,
    ValidatorPage,
    OnewayPage,
    MulticityPage,
    RoundtripPage,
    OnewaycabPage,
    RoundtripcabPage,
    ExfeedbackPage,
    //SettingPage,
    HighlightsPage,
    TravelattractionPage,
    TravelpackagePage,
    TopdestinationPage,
    TourdiaryspecialPage,
    ThingstodoPage,
    ReservationPage,
    SavedplacesPage,
    FooddrinkPage,
    DayplansPage,
    GettingaroundsPage,
    PackrequPage,
    PasswordResetPage,
    ProfilePage,
    FlightSearchResultPage,
    CabSearchResultPage,
    HotelSearchResultPage,
    BusSearchResultPage,
    TrainSearchResultPage,
    ShowpackagePage,
    HillstationPage,
    HoneymoonPage,
    PackagePage,
    YogaPage,
    WildlifePage,
    BeachesPage,
    HeritagePage,
    PaymentPage,
    CardholderPage,
    PackageSerchResultPage,
    PackagedetailsPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    AuthProvider,
    HttpModule,
    RestApiProvider,
    DataBaseProvider,
    Camera,
    AngularFireDatabase
  ]
})
export class AppModule {}
  