import { Component, ViewChild } from '@angular/core';
import { Nav, Platform } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';

import { LoginPage } from '../pages/login/login';
import { HomePage } from '../pages/home/home';
import { TabPage } from '../pages/tab/tab';
import { SingupPage } from '../pages/singup/singup';
import { ExfeedbackPage } from '../pages/exfeedback/exfeedback';
import { PackrequPage } from '../pages/packrequ/packrequ';
import { ProfilePage } from '../pages/profile/profile';

import { AngularFireAuth } from 'angularfire2/auth';
import { FlightsPage } from '../pages/flights/flights';
import { FlightSearchResultPage } from '../pages/flight-search-result/flight-search-result';
import { BusSearchResultPage } from '../pages/bus-search-result/bus-search-result';
import { TrainSearchResultPage } from '../pages/train-search-result/train-search-result';
import { HotelSearchResultPage } from '../pages/hotel-search-result/hotel-search-result';
import { CabSearchResultPage } from '../pages/cab-search-result/cab-search-result';
import { ShowpackagePage } from '../pages/showpackage/showpackage';
import { BusPage } from '../pages/Bus/Bus';
import { TravelattractionPage } from '../pages/travelattraction/travelattraction';
import { WildlifePage } from '../pages/wildlife/wildlife';
import { HillstationPage } from '../pages/hillstation/hillstation';
import { BeachesPage } from '../pages/beaches/beaches';
import { HeritagePage } from '../pages/heritage/heritage';
import { YogaPage } from '../pages/yoga/yoga';
import { HoneymoonPage } from '../pages/honeymoon/honeymoon';
import { PaymentPage } from '../pages/payment/payment';
import { CardholderPage } from '../pages/cardholder/cardholder';
import { TopdestinationPage } from '../pages/topdestination/topdestination';
import { TravelpackagePage } from '../pages/travelpackage/travelpackage';
import { PackageSerchResultPage } from '../pages/package-serch-result/package-serch-result';
import { PackagePage } from '../pages/package/package';


@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

 rootPage: any = TabPage;

  pages: Array<{ title: string, component: any }>;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen,private afAuth: AngularFireAuth) {

    this.pages = [
      { title: 'Home', component: HomePage },
      { title: 'Experiance Feedbak', component: ExfeedbackPage },
      { title: 'Package requirements', component: PackrequPage },
      { title: 'Packages', component: PackagePage },
      { title: 'Profile', component: ProfilePage }, 
      { title: 'Login', component: LoginPage }
    ];

  }

  initializeApp() {
    this.platform.ready().then(() => {
      this.statusBar.styleDefault();
      this.splashScreen.hide();
    });
  }

  openPage(page) {
    this.nav.push(page.component);
  }
  logout() {
    this.afAuth.auth.signOut();
    this.nav.setRoot(LoginPage);
  }

}
